#ifndef CONSOLEAPP_VECTOR_H
#define CONSOLEAPP_VECTOR_H

#include <iostream>
#include <math.h>

class Vector {
private:
    double x;
    double y;
    double z;
public:
    Vector():x(1),y(2),z(3)
    {}
    Vector(double x_, double y_, double z_):x(x_),y(y_),z(z_)
    {}
    void show(){
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }
    double getModule(){
        return sqrt((x*x) + (y*y) + (z*z));
    }
};


#endif //CONSOLEAPP_VECTOR_H
